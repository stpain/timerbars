


TimerBars = {}

SLASH_TIMERBARS1 = '/timerbars'
SlashCmdList['TIMERBARS'] = function(msg)

	if msg == '-help' then
		print('help')
	end

end

--main frame for addon
TimerBars.MainFrame = CreateFrame('FRAME', 'TimerBarsMainFrame', UIParent)
--register events
TimerBars.MainFrame:RegisterEvent('ADDON_LOADED')
TimerBars.MainFrame:RegisterEvent('UNIT_SPELLCAST_START')
TimerBars.MainFrame:RegisterEvent('UNIT_SPELLCAST_CHANNEL_START')
TimerBars.MainFrame:RegisterEvent('UNIT_SPELLCAST_STOP')
TimerBars.MainFrame:RegisterEvent('UNIT_SPELLCAST_FAILED')

TimerBars.Icons = {}

--TimerBars.IconSize = 40

--TimerBars.SpellsTable = {}

TimerBars.SpellsOptionList = {}

TimerBars.ClassColours = {
	DEATHKNIGHT = { r = 0.77, g = 0.12, b = 0.23, fs = '|cffC41F3B' },
	DRUID = { r = 1.00, g = 0.49, b = 0.04, fs = '|cffFF7D0A' },
	HUNTER = { r = 0.67, g = 0.83, b = 0.45, fs = '|cffABD473' },
	MAGE = { r = 0.25, g = 0.78, b = 0.92, fs = '|cff40C7EB' },
	PALADIN = { r = 0.96, g = 0.55, b = 0.73, fs = '|cffF58CBA' },
	PRIEST = { r = 1.00, g = 1.00, b = 1.00, fs = '|cffFFFFFF' },
	ROGUE = { r = 1.00, g = 0.96, b = 0.41, fs = '|cffFFF569' },
	SHAMAN = { r = 0.00, g = 0.44, b = 0.87, fs = '|cff0070DE' },
	WARLOCK = { r = 0.53, g = 0.53, b =	0.93, fs = '|cff8787ED' },
	WARRIOR = { r = 0.78, g = 0.61, b = 0.43, fs = '|cffC79C6E' },
}

function TimerBars.PrintMessage(msg)
	local c = nil
	if UnitClass('player') == 'DEATH KNIGHT' then
		c = 'DEATHKNIGHT'
	else
		c = UnitClass('player')
	end
	
	print(tostring(TimerBars.ClassColours[string.upper(c)].fs..'TimerBars: '..msg))
	
end

function TimerBars.MakeFrameMovable(frame)
	frame:SetMovable(true)
	frame:EnableMouse(true)
	frame:RegisterForDrag("LeftButton")
	frame:SetScript("OnDragStart", frame.StartMoving)
	frame:SetScript("OnDragStop", frame.StopMovingOrSizing)
end

function TimerBars.LockFramePos(frame)
	frame:SetMovable(false)
end

----------------------------------------------------------------------------------------------------
-- options frame
----------------------------------------------------------------------------------------------------

--options interface
TimerBars.OptionsPanel = CreateFrame("Frame", "TimerBarsOptionsPanel", UIParent);
TimerBars.OptionsPanel.name = "TimerBars";
InterfaceOptions_AddCategory(TimerBars.OptionsPanel)
-- TimerBars.OptionsPanel:SetBackdrop({ --bgFile = "Interface/Tooltips/UI-Tooltip-Background", 
                                            -- edgeFile = "Interface/Tooltips/UI-Tooltip-Border", 
                                            -- tile = true, tileSize = 16, edgeSize = 20, 
                                            -- insets = { left = 4, right = 4, top = 4, bottom = 4 }});
--TimerBars.OptionsPanel:SetBackdropColor(0,0,0,0.5)
TimerBars.OptionsPanel.texture = TimerBars.OptionsPanel:CreateTexture('$parent_Texture', 'BACKGROUND')
--TimerBars.OptionsPanel.texture:SetAllPoints(TimerBars.OptionsPanel)
TimerBars.OptionsPanel.texture:SetPoint('TOPLEFT', 4, -4)
TimerBars.OptionsPanel.texture:SetPoint('BOTTOMRIGHT', -4, 4)

--header
TimerBars.OptionsPanel.Header = TimerBars.OptionsPanel:CreateFontString('TimerBarsOptionsPanelHeader', 'OVERLAY', 'GameFontNormal')
TimerBars.OptionsPanel.Header:SetText('Timer Bars')
TimerBars.OptionsPanel.Header:SetPoint('TOPLEFT', 16, -16)
TimerBars.OptionsPanel.Header:SetFont("Fonts\\FRIZQT__.TTF", 16)

--summary text
TimerBars.OptionsPanel.SummaryText = TimerBars.OptionsPanel:CreateFontString('TimerBarsOptionsPanelSummaryText', 'OVERLAY', 'GameFontNormal')
TimerBars.OptionsPanel.SummaryText:SetPoint('TOPLEFT', 16, -40)
TimerBars.OptionsPanel.SummaryText:SetText('Click on the spells or abilities you want to be tracked in the HUD bar.')
TimerBars.OptionsPanel.SummaryText:SetTextColor(1,1,1,1)
TimerBars.OptionsPanel.SummaryText:SetJustifyV("TOP")
TimerBars.OptionsPanel.SummaryText:SetJustifyH("LEFT")
TimerBars.OptionsPanel.SummaryText:SetSize(400, 50)

--load button

TimerBars.OptionsPanel.LoadSpellsButton = CreateFrame("BUTTON", "TimerBarsLoadSpellsButton", TimerBars.OptionsPanel, "UIPanelButtonTemplate")
TimerBars.OptionsPanel.LoadSpellsButton:SetText('|cFFFFFFFFRefresh Spells|r')
TimerBars.OptionsPanel.LoadSpellsButton:SetPoint("TOPLEFT", 20, -75)
TimerBars.OptionsPanel.LoadSpellsButton:SetSize(120, 22)
TimerBars.OptionsPanel.LoadSpellsButton:SetScript('OnClick', function() TimerBars.LoadSpells() end)

--hud length
TimerBars.OptionsPanel.HUDLengthSlider = CreateFrame("Slider", "TimerBars_HUDLengthSlider", TimerBars.OptionsPanel, "OptionsSliderTemplate")
TimerBars.OptionsPanel.HUDLengthSlider:SetThumbTexture("Interface\\Buttons\\UI-SliderBar-Button-Horizontal")
TimerBars.OptionsPanel.HUDLengthSlider:SetSize(100, 20)
TimerBars.OptionsPanel.HUDLengthSlider:SetOrientation('HORIZONTAL')
TimerBars.OptionsPanel.HUDLengthSlider:SetPoint('TOPRIGHT', -144, -55)
TimerBars.OptionsPanel.HUDLengthSlider:SetMinMaxValues(100, 600)
TimerBars.OptionsPanel.HUDLengthSlider:SetValueStep(1)
--get value when changed
TimerBars.OptionsPanel.HUDLengthSlider:SetScript('OnValueChanged', function(self) 
	getglobal(TimerBars.OptionsPanel.HUDLengthSlider:GetName() .. 'Text'):SetText(string.format('%.0f', tostring(self:GetValue())))
	TimerBarsGlobalSettings[UnitGUID('player')].HUDLength = self:GetValue()
	TimerBars.HUD:SetSize(self:GetValue(), TimerBars.OptionsPanel.IconSizeSlider:GetValue())
end)
TimerBars.OptionsPanel.HUDLengthSlider.tooltipText = 'HUD Length' --Creates a tooltip on mouseover.
getglobal(TimerBars.OptionsPanel.HUDLengthSlider:GetName() .. 'Low'):SetText('100')
getglobal(TimerBars.OptionsPanel.HUDLengthSlider:GetName() .. 'High'):SetText('600')
getglobal(TimerBars.OptionsPanel.HUDLengthSlider:GetName() .. 'Text'):SetText('HUD Length')
--TimerBars.OptionsPanel.HUDLengthSlider:SetValue(40)

--icon size
TimerBars.OptionsPanel.IconSizeSlider = CreateFrame("Slider", "TimerBars_HUDIconSizeSlider", TimerBars.OptionsPanel, "OptionsSliderTemplate")
TimerBars.OptionsPanel.IconSizeSlider:SetThumbTexture("Interface\\Buttons\\UI-SliderBar-Button-Horizontal")
TimerBars.OptionsPanel.IconSizeSlider:SetSize(100, 20)
TimerBars.OptionsPanel.IconSizeSlider:SetOrientation('HORIZONTAL')
TimerBars.OptionsPanel.IconSizeSlider:SetPoint('TOPRIGHT', -24, -55)
TimerBars.OptionsPanel.IconSizeSlider:SetMinMaxValues(10, 100)
TimerBars.OptionsPanel.IconSizeSlider:SetValueStep(1)
--get value when changed
TimerBars.OptionsPanel.IconSizeSlider:SetScript('OnValueChanged', function(self) 
	getglobal(TimerBars.OptionsPanel.IconSizeSlider:GetName() .. 'Text'):SetText(string.format('%.0f', tostring(self:GetValue())))
	TimerBarsGlobalSettings[UnitGUID('player')].IconSize = self:GetValue()
	TimerBars.HUD:SetSize(TimerBars.OptionsPanel.HUDLengthSlider:GetValue(), self:GetValue())
end)
TimerBars.OptionsPanel.IconSizeSlider.tooltipText = 'Icon Size' --Creates a tooltip on mouseover.
getglobal(TimerBars.OptionsPanel.IconSizeSlider:GetName() .. 'Low'):SetText('10')
getglobal(TimerBars.OptionsPanel.IconSizeSlider:GetName() .. 'High'):SetText('100')
getglobal(TimerBars.OptionsPanel.IconSizeSlider:GetName() .. 'Text'):SetText('Icon Size')
--TimerBars.OptionsPanel.IconSizeSlider:SetValue(40)


--SCROLL FRAME
TimerBars.OptionsPanel.SpellsScrollFrame = CreateFrame('ScrollFrame', 'SpellsScrollFrame', TimerBars.OptionsPanel)
--TimerBars.OptionsPanel.SpellsScrollFrame:SetPoint('TOPLEFT', 0, -100)
--TimerBars.OptionsPanel.SpellsScrollFrame:SetPoint('BOTTOMLEFT', -4, 4)
TimerBars.OptionsPanel.SpellsScrollFrame:SetSize(625, 400)
--TimerBars.OptionsPanel.SpellsScrollFrame:SetAllPoints(TimerBars.OptionsPanel)
TimerBars.OptionsPanel.SpellsScrollFrame:SetPoint('TOPLEFT', 0, -160)
--TimerBars.OptionsPanel.scrollframe = TimerBars.OptionsPanel.SpellsScrollFrame

--SCROLLBAR
TimerBars.OptionsPanel.SpellsScrollBar = CreateFrame("Slider", 'SpellsScrollBar', TimerBars.OptionsPanel.SpellsScrollFrame, "UIPanelScrollBarTemplate")
--TimerBars.OptionsPanel.SpellsScrollBar:SetPoint("TOPLEFT", TimerBars.OptionsPanel.SpellsScrollFrame, "TOPRIGHT", -6, -6) 
--TimerBars.OptionsPanel.SpellsScrollBar:SetPoint("BOTTOMLEFT", TimerBars.OptionsPanel.SpellsScrollFrame, "BOTTOMRIGHT", -6, 6)
TimerBars.OptionsPanel.SpellsScrollBar:SetPoint("TOPRIGHT", -8, -16) 
TimerBars.OptionsPanel.SpellsScrollBar:SetPoint("BOTTOMRIGHT", -8, 16)
TimerBars.OptionsPanel.SpellsScrollBar:SetMinMaxValues(1, 100)
TimerBars.OptionsPanel.SpellsScrollBar:SetValueStep(1)
TimerBars.OptionsPanel.SpellsScrollBar.scrollStep = 1
TimerBars.OptionsPanel.SpellsScrollBar:SetValue(1)
TimerBars.OptionsPanel.SpellsScrollBar:SetWidth(16)
TimerBars.OptionsPanel.SpellsScrollBar:SetScript('OnValueChanged', function(self, value) 

	self:GetParent():SetVerticalScroll(((TimerBars.OptionsPanel.SpellsScrollFrameContent:GetHeight() - TimerBars.OptionsPanel.SpellsScrollFrame:GetHeight()) / 100 ) * value)

	if value == 1 then
		self:GetParent():SetVerticalScroll(1)
	end
end )
--mouse scroll wheel function
TimerBars.OptionsPanel.SpellsScrollFrame:EnableMouseWheel(1)
TimerBars.OptionsPanel.SpellsScrollFrame:SetScript('OnMouseWheel', function(self, value) 
local v_mouse_scroll = TimerBars.OptionsPanel.SpellsScrollBar:GetValue()
	if value == 1 then
		TimerBars.OptionsPanel.SpellsScrollBar:SetValue(v_mouse_scroll - 1)
		--print(self:GetVerticalScroll())
	elseif value == -1 then
		TimerBars.OptionsPanel.SpellsScrollBar:SetValue(v_mouse_scroll + 1)
		--print(self:GetVerticalScroll())
	end
end)


local scrollBarTexture = TimerBars.OptionsPanel.SpellsScrollBar:CreateTexture('SpellsScrollBarTexture', 'BACKGROUND')
scrollBarTexture:SetAllPoints(TimerBars.OptionsPanel.SpellsScrollBar)
scrollBarTexture:SetTexture(0, 0, 0, 0.4)
TimerBars.OptionsPanel.scrollbar = scrollBarTexture

--SCROLL FRAME CONTENT FRAME
TimerBars.OptionsPanel.SpellsScrollFrameContent = CreateFrame('FRAME', 'SpellScrollFrameContent', TimerBars.OptionsPanel.SpellsScrollFrame)
--TimerBars.OptionsPanel.SpellsScrollFrameContent:SetSize(200, 1000)

TimerBars.OptionsPanel.SpellsScrollFrame.content = TimerBars.OptionsPanel.SpellsScrollFrameContent
TimerBars.OptionsPanel.SpellsScrollFrame:SetScrollChild(TimerBars.OptionsPanel.SpellsScrollFrameContent)


-- experimental grid view
local gridViewItemWidth = 195
local gridViewItemHeight = 40
local gridViewItemPadding = 8
local GridViewItems = {}
function TimerBars.LoadGridView()

	if TimerBarsGlobalSettings[UnitGUID('player')].SpellsTable ~= nil then
		local i, t = 1, #TimerBarsGlobalSettings[UnitGUID('player')].SpellsTable

		for row = 1, math.ceil(t/3) do

			for col = 1, 3 do

				if TimerBarsGlobalSettings[UnitGUID('player')].SpellsTable[i] and GridViewItems[i] == nil then

					local f = CreateFrame("FRAME", tostring("TimerbarsGridViewItem_"..col..'_'..row), TimerBars.OptionsPanel.SpellsScrollFrameContent) --, "TooltipBorderedFrameTemplate")
					f:SetSize(gridViewItemWidth, gridViewItemHeight)
					f:SetPoint("TOPLEFT", ((col - 1) * gridViewItemWidth) + gridViewItemPadding, (((row - 1) * gridViewItemHeight) + gridViewItemPadding) * -1)
					f:SetBackdrop({ bgFile = "Interface/Tooltips/UI-Tooltip-Background", 
						--edgeFile = "Interface/Tooltips/UI-Tooltip-Border", 
						--tile = true, tileSize = 16, edgeSize = 16, 
						insets = { left = 2, right = 2, top = 2, bottom = 2 }
						});
					f:SetBackdropColor(0, 0, 0, 0.7)

					f.spell = TimerBarsGlobalSettings[UnitGUID('player')].SpellsTable[i].Spell
					--print(f.spell)
					
					f.icon = CreateFrame("FRAME", tostring("$parent"..'_icon'), f)
					f.icon:SetPoint("RIGHT", -2, 0)
					f.icon:SetSize(gridViewItemHeight * 0.9, gridViewItemHeight * 0.9) -- set the icon to have a 5% margin top+bottom
					
					f.icon.t = f.icon:CreateTexture("$parent_Background", "BACKGROUND")
					f.icon.t:SetTexture(TimerBarsGlobalSettings[UnitGUID('player')].SpellsTable[i].Icon)
					f.icon.t:SetAllPoints(f.icon)

					f.cb = CreateFrame("CheckButton", tostring('TimerBarsOptionsListCheckBox_'..TimerBarsGlobalSettings[UnitGUID('player')].SpellsTable[i].Spell), f, "ChatConfigCheckButtonTemplate")
					f.cb:SetScript('OnClick', function() 
						for k, spell in pairs(TimerBarsGlobalSettings[UnitGUID('player')].SpellsTable) do --loop table as spells will be added over time
							if spell.Spell == f.spell then
								spell.Watch = f.cb:GetChecked()
								print('Watching', f.spell, f.cb:GetChecked())
							end
						end
					end)
					f.cb:SetChecked(TimerBarsGlobalSettings[UnitGUID('player')].SpellsTable[i].Watch)
					f.cb:SetPoint('LEFT', 2, 0)
					_G[f.cb:GetName().."Text"]:SetText(TimerBarsGlobalSettings[UnitGUID('player')].SpellsTable[i].Spell)
					_G[f.cb:GetName()..'Text']:SetTextColor(1,1,1,1)
					_G[f.cb:GetName()..'Text']:SetFont("Fonts\\FRIZQT__.TTF", 10)
					_G[f.cb:GetName()..'Text']:SetSize( 100, (gridViewItemHeight * 0.9))
		
					TimerBars.OptionsPanel.SpellsScrollFrameContent:SetSize((600), (row * gridViewItemHeight) + 4)
					
					TimerBars.SpellsOptionList[TimerBarsGlobalSettings[UnitGUID('player')].SpellsTable[i].Spell] = cb

					GridViewItems[i] = f
					i = i + 1
				end
			end
		end

	end
end

----------------------------------------------------------------------------------------------------
-- HUD frame
----------------------------------------------------------------------------------------------------

--HUD frame for icons
TimerBars.HUD = CreateFrame('FRAME', 'TimerBarsHUD', UIParent)
TimerBars.HUD:SetBackdrop({ bgFile = "Interface/Tooltips/UI-Tooltip-Background", 
                                            --edgeFile = "Interface/Tooltips/UI-Tooltip-Border", 
                                            tile = true, tileSize = 16, edgeSize = 20, 
                                            insets = { left = 4, right = 4, top = 4, bottom = 4 }});
TimerBars.HUD:SetBackdropColor(0,0,0,0.3)
TimerBars.MakeFrameMovable(TimerBars.HUD)
TimerBars.HUD:SetPoint('CENTER', 0, 0)
TimerBars.HUD:SetSize(300, 40)
----------------------------------------------------------------------------------------------------

function TimerBars.LoadSpells()

	local spellsCount = #TimerBarsGlobalSettings[UnitGUID('player')].SpellsTable or 0


	if TimerBars.Version < 4 then
		TimerBars.OptionsPanel.texture:SetTexture(TimerBars.GetTalentTreeTexture_Classic())
	else
		TimerBars.OptionsPanel.texture:SetTexture(TimerBars.GetTalentTreeTexture_Retail())
	end

	for tab = 1, GetNumSpellTabs() do
		local tabName, tabTexture, tabOffset, numEntries = GetSpellTabInfo(tab)
		for i=tabOffset + 1, tabOffset + numEntries do
			local spellName, spellSubName = GetSpellBookItemName(i, BOOKTYPE_SPELL)
			local t = GetSpellBookItemTexture(i, BOOKTYPE_SPELL)
			if spellName then
				local add = true
				if TimerBarsGlobalSettings[UnitGUID('player')].SpellsTable == nil then
					table.insert( TimerBarsGlobalSettings[UnitGUID('player')].SpellsTable, { Spell = spellName, CD = 0, Icon = t, Watch = false } )
				else
					for k, spell in ipairs(TimerBarsGlobalSettings[UnitGUID('player')].SpellsTable) do
						if spell.Spell == spellName then
							add = false
						end
					end
					if add == true then
						table.insert( TimerBarsGlobalSettings[UnitGUID('player')].SpellsTable, { Spell = spellName, CD = 0, Icon = t, Watch = false } )
						spellsCount = spellsCount + 1
					end
				end
			end
			i = i + 1
		end
	end

	print('new spells', spellsCount)
end

function TimerBars.GetTalentTreeTexture_Retail()
	local tab_name, _, _, _ = GetSpellTabInfo(2)
	local c = UnitClass('player')
	return tostring('Interface/TalentFrame/'..c..tab_name..'-TopLeft')
end

function TimerBars.GetTalentTreeTexture_Classic()
	local current_talents = {}
	for i = 1, 3 do
		table.insert( current_talents, { Name = select(1, GetTalentTabInfo(i, nil, nil, nil)), Icon = select(2, GetTalentTabInfo(i, nil, nil, nil)), Points = select(3, GetTalentTabInfo(i, nil, nil, nil)), Background = select(4, GetTalentTabInfo(i, nil, nil, nil)) } )
	end
	table.sort(current_talents, function(a, b) return a.Points > b.Points end)
	for p = 1, 3 do
		if current_talents[p].Points > 0 then
			has_spec = true
		end
	end
	return tostring('Interface/TalentFrame/'..current_talents[1].Background..'-TopLeft')
end


function TimerBars.OnUpdate()
	for k, s in ipairs(TimerBarsGlobalSettings[UnitGUID('player')].SpellsTable) do	
		if s.Watch == true then		
			if TimerBars.Icons[s.Spell] then
				TimerBars.Icons[s.Spell]:Hide()
			end	
			local start, duration, enabled, modRate = GetSpellCooldown(s.Spell)
			if duration > 1.5 then			
				if TimerBars.Icons[s.Spell] == nil then				
					local f = CreateFrame('FRAME', 'Icons'..s.Spell, TimerBars.HUD)
					f:SetPoint('TOPLEFT', 0, 0)
					f:SetSize(TimerBarsGlobalSettings[UnitGUID('player')].IconSize, TimerBarsGlobalSettings[UnitGUID('player')].IconSize)
					f.t = f:CreateTexture('$parent_Texture', 'BACKGROUND')
					f.t:SetTexture(s.Icon)
					f.t:SetAllPoints(f)
					TimerBars.Icons[s.Spell] = f
				end
				local r = ((start + duration) - GetTime())				
				TimerBars.Icons[s.Spell]:Show()
				TimerBars.Icons[s.Spell]:SetPoint('TOPLEFT', ((300 / duration) * r), 0)
			end			
		end		
	end	
end

function TimerBars.LoadSettings()

	local version, build, date, tocversion = GetBuildInfo()
	
	TimerBars.Version = tonumber(string.sub(version, 1,1))

	--apply new settings
	if TimerBarsGlobalSettings[UnitGUID('player')] ~= nil then
		if TimerBarsGlobalSettings[UnitGUID('player')].HUDLength == nil then TimerBarsGlobalSettings[UnitGUID('player')].HUDLength = 300.0 end
	end

	if TimerBarsGlobalSettings[UnitGUID('player')] == nil then
		TimerBarsGlobalSettings[UnitGUID('player')] = { SpellsTable = {}, IconSize = 40.0, HUDLength = 300.0 }
	else
		TimerBars.OptionsPanel.IconSizeSlider:SetValue(TimerBarsGlobalSettings[UnitGUID('player')].IconSize)
		TimerBars.OptionsPanel.HUDLengthSlider:SetValue(TimerBarsGlobalSettings[UnitGUID('player')].HUDLength)
	end

	TimerBars.LoadSpells()

	TimerBars.LoadGridView()

end


function TimerBars.OnEvent(self, event, ...)

	if event == 'ADDON_LOADED' and select(1, ...) == 'TimerBars' then
		print('TimerBars loaded!')

		if TimerBarsGlobalSettings == nil then
			TimerBarsGlobalSettings = {}
		end
		local t = C_Timer.After(1, function() TimerBars.LoadSettings() end)
	end
	
end




TimerBars.MainFrame:SetScript('OnEvent', TimerBars.OnEvent)
TimerBars.MainFrame:SetScript('OnUpdate', TimerBars.OnUpdate)
